$(function () {

//===== Common animations ======
  //Preloader
  setTimeout(function(){
    $('body').css('opacity', 1);
  }, 500);


  //Lazy load
  if($('.js-home-page').length || $('.js-works-page').length || $('.js-karman-page').length || $('.js-director-page').length || $('.js-legal-page').length)  {
    const observer = lozad(); // lazy loads elements with default selector as '.lozad'

    observer.observe();
  }


  //Cookies
  if (localStorage.getItem('cookieSeen') != 'shown') {
    $('.js-cookies-banner').delay(3000).fadeIn();
  };

  $('.js-cookies-accept').click(function() {
    $('.js-cookies-banner').fadeOut();
    localStorage.setItem('cookieSeen','shown');
  });

  $('.js-cookies-close').click(function() {
    $('.js-cookies-banner').fadeOut();
  });


  //Hide video cover onClick
  $('.js-video-cover').on('click', function(){
    $(this).fadeOut();
    const iframe = $(this).next();
    iframe.addClass('is-visible');
    const player = new Vimeo.Player(iframe);
    player.play();
  });


  //Hide blur after video will preload
  var videos = document.querySelectorAll('video');

  videos.forEach(function (v) {
    v.pause();

    v.addEventListener('canplay', function () {
      v.loop = true;

      if(v.previousElementSibling) {
        v.previousElementSibling.classList.add('is-visible');
      }

      v.play();
    });
  });


  //Burger menu
  $('.js-burger').on('click', function(){
    $(this).toggleClass('is-active');
    $('body').toggleClass('body-overflow');
    $('.js-menu').toggleClass('is-active');
  });

  if($(window).width() < 960) {
    $('.js-menu a').on('click', function(){
      $('.js-burger').toggleClass('is-active');
      $('body').toggleClass('body-overflow');
      $('.js-menu').toggleClass('is-active');
    })
  }


  //Smooth anchor scroll
  $("a[href*='#']").mPageScroll2id();


  //Play video on reveal
  if($('.js-video').length) {
    let video = gsap.utils.toArray('.js-video');

    video.forEach(function(elem) {

      ScrollTrigger.create({
        trigger: elem,
        start: "top 95%",
        // markers: true,
        onEnter: function() { elem.play(); }, 
        onEnterBack: function() { elem.play(); }, 
        onLeave: function() { elem.pause(); },
        onLeaveBack: function() { elem.pause(); },
      });
    });
  }


  //Video nav
  $(".js-mute-video").each(function () {
    $(this).on('click', function(){
      let currentVideo = $(this).parent().siblings('.js-video');

      if (currentVideo.prop('muted')) {
          currentVideo.prop('muted', false);
          $(this).find('svg').removeClass('is-muted');

      } else {
        currentVideo.prop('muted', true);
        $(this).find('svg').addClass('is-muted');
      }
    })
  });

  $(".js-restart-video").each(function () {
    $(this).on('click', function(){
      let currentVideo = $(this).parent().siblings('.js-video')[0];

      currentVideo.currentTime = 0;
      currentVideo.play();
    })
  });





//===== Homepage general animations ======
  //Spati img reveal
  // if ($(".js-spati-imgs-box").length && $(window).width() > 1200) {
  if ($(".js-spati-imgs-box").length) {
    let spatiImgsTween = new TimelineMax({paused: true});

    spatiImgsTween
      .add('start')
      .fromTo(".js-spati-img1", 1, { x: '25rem', rotation: '45deg', ease: Power1.ease }, { x: '-50rem', rotation: '270deg', ease: Power1.ease }, 'start')
      .fromTo(".js-spati-img2", 1, { x: '75rem', rotation: '180deg', ease: Power1.ease }, { x: '-100rem', rotation: '360deg', ease: Power1.ease }, 'start')
      .fromTo(".js-spati-img3", 1, { x: '125rem', rotation: '270deg', ease: Power1.ease }, { x: '-150rem', rotation: '450deg', ease: Power1.ease }, 'start')
    ;

    ScrollTrigger.create({
      trigger: '.js-spati',
      start: "0% 60%",
      end: "100% 0%",
      animation: spatiImgsTween,
      scrub: true,
      // markers: true,
    });
  }


  //Video nav for Nike project
  if($('.js-nike-video-iframe').length) {
    const nikeIframe = $('.js-nike-video-iframe');
    const nikePlayer = new Vimeo.Player(nikeIframe);
  
    let nikeMuted = false;
  
    function nikeToggle() {
  
      nikeMuted = !nikeMuted;
  
      if (nikeMuted) {
        $('.js-nike-mute-video svg').addClass('is-muted');
        nikePlayer.setMuted(true);
      } else {
        $('.js-nike-mute-video svg').removeClass('is-muted');
        nikePlayer.setMuted(false);
      }
  
    }
  
    $('.js-nike-mute-video').on('click', nikeToggle);
  
    function nikeUnload() {
      nikePlayer.unload();
      nikePlayer.play();
    }
  
    $('.js-nike-restart-video').on('click', nikeUnload);
  }


  //Video nav for Jupiter project
  if($('.js-jupiter-video-iframe').length) {
    const jupiterIframe = $('.js-jupiter-video-iframe');
    const jupiterPlayer = new Vimeo.Player(jupiterIframe);
  
    let jupiterMuted = false;
  
    function jupiterToggle() {
  
      jupiterMuted = !jupiterMuted;
  
      if (jupiterMuted) {
        $('.js-jupiter-mute-video svg').addClass('is-muted');
        jupiterPlayer.setMuted(true);
      } else {
        $('.js-jupiter-mute-video svg').removeClass('is-muted');
        jupiterPlayer.setMuted(false);
      }
  
    }
  
    $('.js-jupiter-mute-video').on('click', jupiterToggle);
  
    function jupiterUnload() {
      jupiterPlayer.unload();
      jupiterPlayer.play();
    }
  
    $('.js-jupiter-restart-video').on('click', jupiterUnload);
  }



  //Desktop homepage hover animations
  if($(window).width() > 1200) {

    //Karman imgs hover
    if ($(".js-karman-img1").length) {
      let karmanImg1Tween = new TimelineMax({paused: true});

      karmanImg1Tween
        .to(".js-karman-img1-box", 1, { x: 0, ease: Power1.easeIn });

      $('.js-karman-img1').hover(function(){
        karmanImg1Tween.play();
      }, function(){
        karmanImg1Tween.reverse();
      })
    }

    if ($(".js-karman-img2").length) {
      let karmanImg2Tween = new TimelineMax({paused: true});

      karmanImg2Tween
        .to(".js-karman-img2-box", 1, { x: '-25rem', ease: Power1.easeIn });

      $('.js-karman-img2').hover(function(){
        karmanImg2Tween.play();
      }, function(){
        karmanImg2Tween.reverse();
      })
    }


    //Nike img hover
    if ($(".js-nike-imgs-box").length) {
      let nikeImgsTween = new TimelineMax({paused: true});

      nikeImgsTween
        .add('start')
        .to(".js-nike-img1", 1.5, { x: '30rem', ease: Power1.ease }, 'start')
        .to(".js-nike-img2", 1.5, { x: '-32rem', ease: Power1.ease }, 'start')
        .to(".js-nike-img3", 1.5, { x: '-94rem', ease: Power1.ease }, 'start')
      ;

      $('.js-nike-imgs-box').hover(function(){
        nikeImgsTween.play();
      }, function(){
        nikeImgsTween.reverse();
      });
    }


    //Terra img hover
    if ($(".js-terra-imgs-box").length) {
      let terraImgsTween = new TimelineMax({paused: true});

      terraImgsTween
        .add('start')
        .to(".js-terra-img1", 1, { x: '-9.5rem', ease: Power1.ease }, 'start')
        .to(".js-terra-img2", 1, { x: '-9.5rem', ease: Power1.ease }, 'start')
        .to(".js-terra-img3", 1, { x: '-33rem', ease: Power1.ease }, 'start')
        .to(".js-terra-img4", 1, { x: '-33rem', ease: Power1.ease }, 'start')
      ;

      $('.js-terra-imgs-box').hover(function(){
        terraImgsTween.play();
      }, function(){
        terraImgsTween.reverse();
      });
    }


    //Cult section
    $('.js-cult-hover-box').hover(function(){
      $('.js-cult-hover').hide();
      $('.js-soon-hover').fadeIn(500);
    }, function(){
      $('.js-soon-hover').hide();
      $('.js-cult-hover').fadeIn(500);
    });


    //Directors link hover
    $('.js-directors-item').hover(function(){
      $(this).addClass('is-hover')

      let directorVideoIndex = $(this).index();
      let directorVideoBox = $('.js-director-video-box').eq(directorVideoIndex);
      let directorVideo = directorVideoBox.children('.js-director-video')[0];

      directorVideo.classList.add('is-active');
      directorVideo.currentTime = 0;
      directorVideo.play();

      // $('.js-director-video-box').addClass('is-active');
      directorVideoBox.fadeIn();
    }, function(){
      $(this).removeClass('is-hover');

      let directorVideoIndex = $(this).index();
      let directorVideoBox = $('.js-director-video-box').eq(directorVideoIndex);
      let directorVideo = directorVideoBox.children('.js-director-video')[0];

      directorVideo.classList.remove('is-active');
      directorVideo.pause();

      // $('.js-director-video-box').removeClass('is-active');
      directorVideoBox.hide();
    });


    //Cursor animation
    // set the starting position of the cursor outside of the screen
    let clientX = -100;
    let clientY = -100;
    const cursor = document.querySelector(".js-cursor");

    const initCursor = () => {
      // add listener to track the current mouse position
      document.addEventListener("mousemove", e => {
        clientX = e.clientX;
        clientY = e.clientY;
      });

      // transform the innerCursor to the current mouse position
      const render = () => {
        cursor.style.transform = `translate(${clientX}px, ${clientY}px)`;
        requestAnimationFrame(render);
      };
      requestAnimationFrame(render);
    };

    initCursor();


    //Hover Discovery cursor
    let hoverDiscoverItem;

    const initDiscover = () => {

      const handleMouseEnter = (e) => {
        // $(cursor).hide();
        cursor.classList.add("is-active", "is-discover");
        hoverDiscoverItem = e.currentTarget;
        // console.log('hoverItem enter');
      };
      
      const handleMouseLeave = () => {
        cursor.classList.remove("is-active", "is-discover");
        // console.log('hoverItem leave');
      };

      // add event listeners to all items
      const hoverItems = document.querySelectorAll(".js-discover");
      hoverItems.forEach(item => {
        item.addEventListener("mouseenter", handleMouseEnter);
        item.addEventListener("mouseleave", handleMouseLeave);
      });
    };

    initDiscover();


    //Hover Play cursor
    let hoverPlayItem;

    const initPlay = () => {

      const handleMouseEnter = (e) => {
        // $(cursor).hide();
        cursor.classList.add("is-active", "is-play");
        hoverPlayItem = e.currentTarget;
        // console.log('hoverItem enter');
      };
      
      const handleMouseLeave = () => {
        cursor.classList.remove("is-active", "is-play");
        // console.log('hoverItem leave');
      };

      // add event listeners to all items
      const hoverItems = document.querySelectorAll(".js-play");
      hoverItems.forEach(item => {
        item.addEventListener("mouseenter", handleMouseEnter);
        item.addEventListener("mouseleave", handleMouseLeave);
      });
    };

    initPlay();


    //Hover Hide cursor
    let hoverProjectVideo;

    const initProjectVideoHide = () => {

      const handleMouseProjectVideoEnter = (e) => {
        // $(cursor).hide();
        cursor.classList.add('is-hidden');
        hoverProjectVideo = e.currentTarget;
        // console.log('hoverProjectVideo enter');
      };
      
      const handleMouseProjectVideoLeave = () => {
        cursor.classList.remove('is-hidden');
        // console.log('hoverProjectVideo leave');
      };

      // add event listeners to all items
      const hoverItems = document.querySelectorAll(".js-hide-cursor-over-vimeo");
      hoverItems.forEach(item => {
        item.addEventListener("mouseenter", handleMouseProjectVideoEnter);
        item.addEventListener("mouseleave", handleMouseProjectVideoLeave);
      });
    };

    initProjectVideoHide();

  };


  //Swooth form
  validate.init({
    disableSubmit: true,
    onSubmit: function() {

        var form_data = $(".js-swoosh-form").serialize(); //собераем все данные из формы
        $.ajax({
            method: "POST", //Метод отправки
            url: "send.php", //путь до php файла отправителя
            data: form_data
        });

        console.log("Form sended");
        $("form").trigger("reset");
        $(".js-swoosh-form-input").remove();
        $(".js-swoosh-form-label").append('<div class="swoosh__input swoosh__input-success js-swoosh-form-input"><span>email was sent</span></div>');

    }
  });





//===== Loading animations ======
  //Loading animations variables
  let moveLength = 30;
  let moveEasing = Power1.easeInOut;
  let moveTime = 0.3;
  let stepTime = 0.1;
  let nameBgRevealTime = 0.3;
  let scrollPointMoveDuration = 0.5;
  let startTrigger = 'top 90%';
  let endScrollLength = 'bottom 60%';


  //Loading Common top animations
  if ($(".js-common-item-top").length && $(window).width()> 1200) {
    gsap.set(".js-common-item-top", {y: moveLength, autoAlpha: 0});

    ScrollTrigger.batch(".js-common-item-top", {
      trigger: '.js-common-item-top',
      start: startTrigger,
      interval: 1,
      onEnter: batch => gsap.to(batch, {autoAlpha: 1, y: 0, ease: moveEasing, stagger: 0.1}),
    });
  }


  //Loading Common animations
  if ($(".js-common-item").length && $(window).width()> 1200) {
    gsap.set(".js-common-item", {y: moveLength, autoAlpha: 0});

    ScrollTrigger.batch(".js-common-item", {
      trigger: '.js-common-item',
      start: startTrigger,
      onEnter: batch => gsap.to(batch, {autoAlpha: 1, y: 0, ease: moveEasing, stagger: 0.1}),
    });
  }


  //Loading Footer animations
  if ($(".js-footer-logo").length && $(window).width()> 1200) {

    //=== Footer
    let footerTween = new TimelineMax({paused: true});

    footerTween
      .add('start')
      .from(".js-footer-logo", 0.3, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-footer-text", 0.3, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.1')
      .from(".js-footer-nav", 0.3, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.2')
      .from(".js-footer-social", 0.3, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-footer-legal", 0.3, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
    ;
  
    ScrollTrigger.create({
      trigger: '.js-footer',
      start: startTrigger,
      animation: footerTween,
      // markers: true,
    });

  }


  //Loading Homepage animations
  if ($(".js-home-page").length && $(window).width()> 1200) {

    let nikeOffsetTop = $('.js-nike').offset().top;
    let karmanPOffsetTop = $('.js-karman-p').offset().top;
    let jupiterOffsetTop = $('.js-jupiter').offset().top;
    let terraOffsetTop = $('.js-terra').offset().top;
    let swooshOffsetTop = $('.js-swoosh').offset().top;
    let spatiOffsetTop = $('.js-spati').offset().top;
    let infoOffsetTop = $('.js-info-we').offset().top;

    //=== Karman project
    let karmanProjectTopTween = new TimelineMax({paused: true});

    karmanProjectTopTween
      .add('start')
      .from(".js-top-info", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-top-header-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
      .from(".js-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
      .from(".js-top-header-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.8')
      .from(".js-karman-video", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.8')
      .from(".js-karman-img1", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.9')
      .from(".js-karman-img2", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
    ;

    // $('.js-karman-video-inner')[0].addEventListener('canplay', function () {
      ScrollTrigger.create({
        trigger: '.js-top-info',
        animation: karmanProjectTopTween,
        // markers: true,
      });
    // });



    let karmanProjectTextTween = new TimelineMax({paused: true});

    karmanProjectTextTween
      .add('start')
      .from(".js-karman-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-karman-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.1')
      .from(".js-karman-name", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.2')
      .from(".js-karman-name-bg", nameBgRevealTime, { scaleX: 0, ease: moveEasing }, 'start+=0.5')
    ;

    ScrollTrigger.create({
        trigger: '.js-karman-name',
        start: startTrigger,
        animation: karmanProjectTextTween,
      });


    //=== Nike project
    let nikeProjectTopTween = new TimelineMax({paused: true, delay: 1});

    nikeProjectTopTween
      .add('start')
      .from(".js-nike-video", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-nike-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-nike-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
      .from(".js-nike-name", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
      .from(".js-nike-directors", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-nike-name-bg", nameBgRevealTime, { scaleX: 0, ease: moveEasing }, 'start+=0.8')
    ;

    ScrollTrigger.create({
      trigger: '.js-nike-video',
      start: startTrigger,
      animation: nikeProjectTopTween,
      // markers: true,
    });


    let nikeProjectMenuTween = new TimelineMax({paused: true});

    nikeProjectMenuTween
      .add('start')
      .from(".js-nike-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-nike-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.1')
      .from(".js-nike-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.1')
    ;

    ScrollTrigger.create({
        trigger: '.js-nike-logo',
        start: startTrigger,
        animation: nikeProjectMenuTween,
        // markers: true,
      });


    let nikeProjectImgsTween = new TimelineMax({paused: true});

    nikeProjectImgsTween
      .add('start')
      .from(".js-nike-img1-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-nike-img2-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.25')
      .from(".js-nike-img3-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
    ;

    ScrollTrigger.create({
        trigger: '.js-nike-imgs-box',
        start: startTrigger,
        animation: nikeProjectImgsTween,
        // markers: true,
      });


    //=== Karman-p project
    let karmanpProjectTopTween = new TimelineMax({paused: true});

    karmanpProjectTopTween
      .add('start')
      .from(".js-karmanp-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-karmanp-link", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
      .from(".js-karmanp-video-small", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
      .from(".js-karmanp-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.25')
      .from(".js-karmanp-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.25')
      .from(".js-karmanp-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.35')
    ;

    ScrollTrigger.create({
      trigger: '.js-karmanp-text',
      start: startTrigger,
      animation: karmanpProjectTopTween,
      // markers: true,
    });


    let karmanpVideoTween = new TimelineMax({paused: true});

    karmanpVideoTween
      .add('start')
      .from(".js-karmanp-video", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
        trigger: '.js-karmanp-video',
        start: startTrigger,
        animation: karmanpVideoTween,
        // markers: true,
      });


    //=== Jupiter project
    let jupiterTween = new TimelineMax({paused: true});

    jupiterTween
      .add('start')
      .from(".js-jupiter-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-jupiter-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-jupiter-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
      .from(".js-jupiter-video", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-jupiter-img1", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
      .from(".js-jupiter-img2", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.7')
      .from(".js-jupiter-img3", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.8')
      .from(".js-jupiter-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
      .from(".js-jupiter-name", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.1')
      .from(".js-jupiter-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.2')
      .from(".js-jupiter-name-bg", nameBgRevealTime, { scaleX: 0, ease: moveEasing }, 'start+=1.3')
    ;

    ScrollTrigger.create({
      trigger: '.js-jupiter-logo',
      start: startTrigger,
      animation: jupiterTween,
      // markers: true,
    });


    let jupiterDirectorsTween = new TimelineMax({paused: true});

    jupiterDirectorsTween
      .add('start')
      .from(".js-jupiter-directors", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
        trigger: '.js-jupiter-directors',
        start: startTrigger,
        animation: jupiterDirectorsTween,
        // markers: true,
      });


    //=== Terra project
    let terraTween = new TimelineMax({paused: true});

    terraTween
      .add('start')
      .from(".js-terra-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-terra-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-terra-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
      .from(".js-terra-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-terra-name", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
      .from(".js-terra-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.7')
      .from(".js-terra-name-bg", nameBgRevealTime, { scaleX: 0, ease: moveEasing }, 'start+=0.8')
      .from(".js-terra-video", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.9')
      .from(".js-terra-img1-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
      .from(".js-terra-img2-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.1')
      .from(".js-terra-img3-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.2')
      .from(".js-terra-img4-box", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.3')
    ;

    ScrollTrigger.create({
      trigger: '.js-terra-logo',
      start: startTrigger,
      animation: terraTween,
      // markers: true,
    });


    let terraDirectorsTween = new TimelineMax({paused: true});

    terraDirectorsTween
      .add('start')
      .from(".js-terra-directors", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
        trigger: '.js-terra-directors',
        start: startTrigger,
        animation: terraDirectorsTween,
        // markers: true,
      });


    //=== Swoosh project
    let swooshTween = new TimelineMax({paused: true});

    swooshTween
      .add('start')
      .from(".js-swoosh", 2, { autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-swoosh-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
      .from(".js-swoosh-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1')
      .from(".js-swoosh-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.1')
      .from(".js-swoosh-img", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.2')
      .from(".js-swoosh-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.3')
      .from(".js-swoosh-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.4')
      .from(".js-swoosh-directors", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.5')
      .from(".js-swoosh-form-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.6')
      .from(".js-swoosh-form-input", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=1.7')
    ;

    ScrollTrigger.create({
        trigger: '.js-swoosh',
        start: startTrigger,
        animation: swooshTween,
        // markers: true,
      });


    //=== Spati project
    let spatiTween = new TimelineMax({paused: true});

    spatiTween
      .add('start')
      .from(".js-spati-logo", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-spati-menu", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-spati-email", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
      .from(".js-spati-video", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-spati-imgs-box", moveTime, { autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
    ;

    ScrollTrigger.create({
      trigger: '.js-spati-logo',
      start: startTrigger,
      animation: spatiTween,
      // markers: true,
    });


    let spatiTextTween = new TimelineMax({paused: true});

    spatiTextTween
      .add('start')
      .from(".js-spati-name", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-spati-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.1')
      .from(".js-spati-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.2')
      .from(".js-spati-directors", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-spati-name-bg", nameBgRevealTime, { scaleX: 0, ease: moveEasing }, 'start+=0.4')
    ;

    ScrollTrigger.create({
        trigger: '.js-spati-name',
        start: startTrigger,
        animation: spatiTextTween,
        // markers: true,
      });


    //=== Info project
    let infoTween1 = new TimelineMax({paused: true});

    infoTween1
      .add('start')
      .from(".js-info-text1", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
      trigger: '.js-info-text1',
      start: startTrigger,
      animation: infoTween1,
      // markers: true,
    });

    let infoTween2 = new TimelineMax({paused: true});

    infoTween2
      .add('start')
      .from(".js-info-text2", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
      trigger: '.js-info-text2',
      start: startTrigger,
      animation: infoTween2,
      // markers: true,
    });


    //=== Contact project
    let contactTween = new TimelineMax({paused: true});

    contactTween
      .add('start')
      .from(".js-contact-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-contact-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.1')
      .from(".js-contact-address", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.2')
      .from(".js-contact-link", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.3')
      .from(".js-contact-img", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.4')
    ;

    ScrollTrigger.create({
      trigger: '.js-contact-title',
      start: startTrigger,
      animation: contactTween,
      // markers: true,
    });


    //=== Directors project
    let directorsTween = new TimelineMax({paused: true});

    directorsTween
      .add('start')
      .from(".js-directors-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-directors-item", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing, stagger: 0.15 }, 'start+=0.25')
    ;

    ScrollTrigger.create({
      trigger: '.js-directors-title',
      start: startTrigger,
      animation: directorsTween,
      // markers: true,
    });


    //=== Cult project
    let cultTween = new TimelineMax({paused: true});

    cultTween
      .add('start')
      .from(".js-cult-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-cult-img", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
    ;

    ScrollTrigger.create({
      trigger: '.js-cult-title',
      start: startTrigger,
      animation: cultTween,
      // markers: true,
    });

  }


  //Loading About page animations
  if ($(".js-about-page").length && $(window).width()> 1200) {

    //=== About top
    let aboutTopTween = new TimelineMax({paused: true});

    aboutTopTween
      .add('start')
      .from(".js-header-black", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-info-text1", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.6')
    ;

    ScrollTrigger.create({
      trigger: '.js-header-black',
      animation: aboutTopTween,
      // markers: true,
    });


    let infoTween2 = new TimelineMax({paused: true});

    infoTween2
      .add('start')
      .from(".js-info-text2", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
      trigger: '.js-info-text2',
      animation: infoTween2,
      // markers: true,
    });


    //=== About Contact
    let contactTween = new TimelineMax({paused: true});

    contactTween
      .add('start')
      .from(".js-contact-title", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
      .from(".js-contact-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.25')
      .from(".js-contact-address", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-contact-img", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.75')
    ;

    ScrollTrigger.create({
      trigger: '.js-contact-title',
      animation: contactTween,
      // markers: true,
    });


    //=== About Team
    gsap.set(".js-team-item", {y: moveLength, autoAlpha: 0});

    ScrollTrigger.batch(".js-team-item", {
      trigger: '.js-team-list',
      start: startTrigger,
      onEnter: batch => gsap.to(batch, {autoAlpha: 1, y: 0, ease: moveEasing, stagger: 0.15}),
    });

  }


  //Loading Works page animations
  if ($(".js-works-page").length && $(window).width()> 1200) {

    //=== Works top
    let worksTopTween = new TimelineMax({paused: true});

    worksTopTween
      .add('start')
      .from(".js-header-white", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start+=0.5')
      .from(".js-work-item1", { y: moveLength, autoAlpha: 0, ease: moveEasing, stagger: 0.15}, 'start+=0.6')
    ;

    ScrollTrigger.create({
      trigger: '.js-header-white',
      animation: worksTopTween,
      // markers: true,
    });


    //=== Works items
    gsap.set(".js-work-item", {y: moveLength, autoAlpha: 0});

    ScrollTrigger.batch(".js-work-item", {
      trigger: '.js-work-item',
      start: startTrigger,
      onEnter: batch => gsap.to(batch, {autoAlpha: 1, y: 0, ease: moveEasing, stagger: 0.1}),
    });


    //=== Works text tween
    let worksTextTween = new TimelineMax({paused: true});

    worksTextTween
      .add('start')
      .from(".js-works-text", moveTime, { y: moveLength, autoAlpha: 0, ease: moveEasing }, 'start')
    ;

    ScrollTrigger.create({
      trigger: '.js-works-text',
      animation: worksTextTween,
      // markers: true,
    });

  }

});
